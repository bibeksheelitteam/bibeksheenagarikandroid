package bibeksheelnagarik.app.www.bibeksheelnagarik.ui.activity;

        import android.Manifest;
        import android.os.Bundle;
        import android.support.annotation.FloatRange;
        import android.support.annotation.Nullable;
        import android.view.View;
        import android.widget.Toast;

        import agency.tango.materialintroscreen.MaterialIntroActivity;
        import agency.tango.materialintroscreen.MessageButtonBehaviour;
        import agency.tango.materialintroscreen.SlideFragmentBuilder;
        import agency.tango.materialintroscreen.animations.IViewTranslation;
        import bibeksheelnagarik.app.www.bibeksheelnagarik.R;
        import bibeksheelnagarik.app.www.bibeksheelnagarik.ui.activity.model.CustomSlide;

public class IntroActivity extends MaterialIntroActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.blue)
                        .buttonsColor(R.color.red)
                        .image(R.drawable.app_logo)
                        .title("Organize your time with us")
                        .description("Would you try?")
                        .build(),
                new MessageButtonBehaviour(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMessage("We provide solutions to make you love your work");
                    }
                }, "Work with love"));

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.blue)
                .buttonsColor(R.color.red)
                .title("Want more?")
                .description("Go on")
                .build());

        addSlide(new CustomSlide());

        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.blue)
                        .buttonsColor(R.color.red)
                        .possiblePermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_SMS})
                        .neededPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
                        .image(R.drawable.app_logo)
                        .title("We provide best tools")
                        .description("ever")
                        .build(),
                new MessageButtonBehaviour(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMessage("Try us!");
                    }
                }, "Tools"));

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.blue)
                .buttonsColor(R.color.red)
                .title("That's it")
                .description("Would you join us?")
                .build());
    }

    @Override
    public void onFinish() {
        super.onFinish();
        Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
    }
}
