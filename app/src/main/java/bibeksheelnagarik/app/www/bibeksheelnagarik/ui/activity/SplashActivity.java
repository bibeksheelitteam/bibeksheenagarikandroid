package bibeksheelnagarik.app.www.bibeksheelnagarik.ui.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onStart() {
        super.onStart();
        final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something
            Log.d("Comments", "First time");
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(new Intent(this, HomeActivity.class))
                    .addNextIntent(new Intent(this, IntroActivity.class))
                    .startActivities();

            // first time task

            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();
        } else {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(new Intent(this, HomeActivity.class))
                    .addNextIntent(new Intent(this, HomeActivity.class))
                    .startActivities();
        }
    }
}